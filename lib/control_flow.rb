# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = ""
  str.chars {|char| result += char if char.downcase != char}
  result
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[str.length / 2]
  else
    str[str.length / 2 - 1,2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count {|char| VOWELS.include?(char)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  (1..num).each {|el| result *= el }
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr.join(separator)
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  str.chars.each_index {|idx|
    if idx.even?
      result += str.downcase[idx]
    else
      result += str[idx].upcase
    end
  }
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(' ')
  result = []
  words.each {|word|
    if word.length >= 5
      result << word.reverse
    else
      result << word
    end
  }
  result.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each {|ele|
    if ele % 3 == 0 && ele % 5 == 0
      result << "fizzbuzz"
    elsif ele % 3 == 0
      result << "fizz"
    elsif ele % 5 == 0
      result << "buzz"
    else
      result << ele
    end
  }
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  i = arr.length - 1
  while i >= 0
    result << arr[i]
    i -= 1
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  (2...num).each {|ele|
    if num % ele == 0
      return false
    end
  }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each {|ele|
    if num % ele == 0
      factors << ele
    end
  }
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  factors_arr = factors(num)
  factors_arr.each {|ele|
    if prime?(ele)
      result << ele
    end
  }
  result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  result = prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
counted = arr.count {|ele| ele % 2 == 0}
  if counted == 1 #there is one even element
    return arr.find_all(&:even?)[0]
  else
    return arr.find_all(&:odd?)[0]
  end
end

oddball([1,2,3,5])
